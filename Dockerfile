# Use Ubuntu 20.04 as the base image
# FROM ubuntu:20.04
FROM condaforge/mambaforge:24.3.0-0

# Update package lists and install Python 3.9
SHELL ["/bin/bash","-l", "-c"]

RUN apt-get update && \
    # apt-get install -y python3.9 && \
    # apt-get install -y python3-pip && \
    apt-get clean

# Install the required packages
RUN apt install -y --no-install-recommends \
    autoconf build-essential ca-certificates \
    gfortran libblas3 libc6 libfftw3-dev libgcc-s1 \
    liblapack-dev wget git

# Set the default Python version to 3.9
# RUN update-alternatives --install /usr/bin/python3 python3 /usr/bin/python3.9 1

# # Create and switch to the home directory of "testuser"
# RUN useradd -ms /bin/bash testuser
# WORKDIR /home/testuser

# # Change to testuser
# USER testuser

ENV workspace=/root/
ENV QE_DIR=$workspace/QE
ENV ASE_DIR=$workspace/ase

RUN git clone --depth 1 --branch qe-7.2 https://gitlab.com/QEF/q-e.git $QE_DIR
WORKDIR $QE_DIR
RUN ./configure
RUN make pw -j4

ENV PATH="$(pwd)/bin:${PATH}"


# # ASE
# COPY --chmod=777 . $ASE_DIR
# WORKDIR $ASE_DIR

# ENV ENVNAME=asetest

# # RUN mamba create -n $ENVNAME python=3.9 -y
# # RUN mamba init && source ~/.bashrc
# # RUN mamba activate $ENVNAME

# RUN python -m venv $workspace/$ENVNAME
# RUN source $workspace/$ENVNAME/bin/activate

# # RUN echo "$(pwd)"
# # RUN echo "$CONDA_PREFIX"
# RUN pip install . pytest==7.0 numpy==1.22
# RUN pip install --user --upgrade git+https://gitlab.com/ase/ase-datafiles.git

# ENV ASE_CONFIG=$workspace/ase.conf
# RUN echo "[executables]" > $ASE_CONFIG
# RUN echo "espresso = $QE_DIR/bin/pw.x" >> $ASE_CONFIG

# ENV DISPLAY=0
# RUN ase test --nogui -c espresso  --pytest -k "espresso" -vvv --ignore=gui/test_run.py